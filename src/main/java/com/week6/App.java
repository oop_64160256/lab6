package com.week6;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {

        BookBank nipitpon = new BookBank("Nipitpon", 50.0); // Constructor
        nipitpon.print();

        BookBank prayud = new BookBank("PraYuddd", 100000.0);
        prayud.print();
        prayud.withdraw(40000.0);
        prayud.print();

        nipitpon.deposit(40000.0);
        nipitpon.print();

        BookBank prawit = new BookBank("Prawit", 1000000.0);
        prawit.print();
        prawit.deposit(2);
        prawit.print();
    }
}
