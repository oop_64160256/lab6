package com.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldDownOver() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.Max_y);
        assertEquals(false, robot.down());
        assertEquals(robot.Max_y, robot.getY());
    }

    @Test
    public void shouldUpNegative() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(false, robot.up());
        assertEquals(robot.Min_y, robot.getY());
    }

    @Test
    public void shouldDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }

    @Test
    public void shouldUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldeftSuccess() {
        Robot robot = new Robot("Robot", 'R', 1, 0);
        assertEquals(true, robot.left());
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldleftNegative() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(false, robot.left());
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldRighttSuccess() {
        Robot robot = new Robot("Robot", 'R', 1, 0);
        assertEquals(true, robot.right());
        assertEquals(2, robot.getX());
    }

    @Test
    public void shouldRighttOver() {
        Robot robot = new Robot("Robot", 'R', 19, 0);
        assertEquals(false, robot.right());
        assertEquals(19, robot.getX());
    }

    @Test
    public void shouldCreateRobotSuccess1() {
        Robot robot = new Robot("Robot", 'R', 13, 13);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(13, robot.getX());
        assertEquals(13, robot.getY());
    }

    @Test
    public void shouldCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldUpNSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 11);
        assertEquals(true, robot.up(5));
        assertEquals(6, robot.getY());
    }

    @Test
    public void shouldUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 0, 11);
        assertEquals(true, robot.up(11));
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldUpNSuccess3() {
        Robot robot = new Robot("Robot", 'R', 0, 11);
        assertEquals(false, robot.up(12));
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldDownNSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 11);
        assertEquals(true, robot.down(5));
        assertEquals(16, robot.getY());
    }

    @Test
    public void shouldDownNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 0, 11);
        assertEquals(true, robot.down(2));
        assertEquals(13, robot.getY());
    }

    @Test
    public void shouldDownNSuccess3() {
        Robot robot = new Robot("Robot", 'R', 0, 11);
        assertEquals(false, robot.down(20));
        assertEquals(19, robot.getY());
    }

    @Test
    public void shouldLeftNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals(true, robot.left(5));
        assertEquals(5, robot.getX());
    }

    @Test
    public void shouldLeftNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 0, 11);
        assertEquals(false, robot.left(2));
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldRightNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        assertEquals(true, robot.right(5));
        assertEquals(15, robot.getX());
    }

    @Test
    public void shouldRightNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 0, 11);
        assertEquals(false, robot.right(23));
        assertEquals(19, robot.getX());
    }

}
