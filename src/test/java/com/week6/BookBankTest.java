package com.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldWithdrawSuccess() {
        BookBank book = new BookBank("Nipitpon", 100.0);
        book.withdraw(50);
        assertEquals(50, book.getBalance(), 0.0001);
    }

    @Test
    public void shouldWithdrawOverBalance() {
        BookBank book = new BookBank("Nipitpon", 100.0);
        book.withdraw(150);
        assertEquals(100, book.getBalance(), 0.0001);
    }
    
    @Test
    public void shouldWithdrawWithNegativeNumber() {
        BookBank book = new BookBank("Nipitpon", 100.0);
        book.withdraw(-10);
        assertEquals(100, book.getBalance(), 0.0001);
    }

    @Test
    public void shouldDepositSuccess() {
        BookBank book = new BookBank("Nipitpon", 100.0);
        book.deposit(25);
        assertEquals(125, book.getBalance(), 0.0001);
    }
    
    @Test
    public void shouldDepositWithNegativeNumber() {
        BookBank book = new BookBank("Nipitpon", 100.0);
        book.deposit(-25);
        assertEquals(100, book.getBalance(), 0.0001);
    }
}
