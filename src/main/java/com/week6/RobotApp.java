package com.week6;

public class RobotApp {
    public static void main(String[] args) {
        Robot kapong = new Robot("Kapong", 'K', 0, 0);
        kapong.print();

        Robot Blue = new Robot("Blue", 'B', 1, 1);
        Blue.print();

        for(int y=Robot.Min_y;y<=Robot.Max_y;y++) {
            for(int x=Robot.Min_x;x<=Robot.Max_x;x++) {
                if (kapong.getY()==y && kapong.getX()==x) {
                    System.out.print(kapong.getSymbol());
                }else if (Blue.getY()==y && Blue.getX()==x) {
                    System.out.print(Blue.getSymbol());
                }else {
                    System.out.print("-");
                }
            }
            System.out.println();
        }

        
    }
}
